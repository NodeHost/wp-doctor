<?php
	
	function goto_test($test,$log){
		echo "<script>window.setTimeout(function(){ window.location.href = \"/wp-doctor/debug_admin_access?test=".$test."&log=".$log."\"; }, 200);</script>";
	}
	
	echo "<h1>Debug WP-Admin Access</h1>Hey, are you having problems getting access to WP admin?<BR>Lets check this out...<BR><BR><BR>";
	
	if (isset($_GET["test"])){
		$log=makesafe($_GET["log"]);
		
		//###########################################################-- 
		//###########################################################-- TEST: start
		//###########################################################-- Lets get some information first!
		//###########################################################-- 
		
		if ($_GET["test"]=="start"){
			echo "<div class='message_normal'>Running test: start</div>";
			write_log("-----------------------------------------",$log);
			
			write_log("Starting Debug WP-Admin Access",$log);
			write_log("We are going to be checking against http://".$system_domain."/wp-admin",$log);
			goto_test("direct_load",$log);
			
			write_log("-----------------------------------------",$log);
		}
		
		//###########################################################-- 
		//###########################################################-- TEST: direct_load
		//###########################################################-- First check, see if we can load it directly!
		//###########################################################-- 
		
		if ($_GET["test"]=="direct_load"){	
			echo "<div class='message_normal'>Running test: direct_load</div>";	
			write_log("-----------------------------------------",$log);
			
			write_log("Starting Test / Direct Load",$log);
			write_log("Fetching http://".$system_domain."/wp-admin",$log);
			$contents=get_page_contents("http://".$system_domain."/wp-admin");
			if ($contents!=""){
				write_log("Got a response from http://".$system_domain."/wp-admin",$log);
			}
			
			if (strpos($contents, 'Powered by WordPress') !== false){
				write_log("We see the Powered by WordPress text on the page",$log);
				goto_test("passed",$log);
			}else{
				goto_test("fail",$log);
			}
			
			write_log("-----------------------------------------",$log);
		}
		
		//###########################################################-- 
		//###########################################################-- TEST: passed
		//###########################################################-- 
		
		if ($_GET["test"]=="passed"){
			echo "<div class='message_good'>Hey we are able to load the Wordpress admin area... If you are having problems connecting our test was not able to find out why. You can view the results below.</div>";
		}
		
		//###########################################################-- 
		//###########################################################-- TEST: fixed
		//###########################################################-- 
		
		if ($_GET["test"]=="fixed"){
			echo "<div class='message_good'>When running our tests we ran into a problem connecting and accessing wp-admin but we have fixed the error. You can view the results below.</div>";
		}
		
		//###########################################################-- 
		//###########################################################-- TEST: fail
		//###########################################################-- 
		
		if ($_GET["test"]=="fail"){
			echo "<div class='message_error'>When running our tests we ran into a problem connecting and accessing wp-admin but we have no fixes.</div>";
		}
		
		
		echo "<div class='bubble'>";
		echo "<h3 style='margin-top:0px;'>Results</h3>";
		echo "<pre><code>".get_log($log)."</code></pre>";
		echo "</div>";
		
	}else{
		
		echo "<a href='/wp-doctor/debug_admin_access?test=start&log=debug_admin_access_".generate_code("10","simple")."'><button>Run tests</button></a>";
		
	}