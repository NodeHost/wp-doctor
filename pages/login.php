<?php

echo "<div style='text-align:center;'><h1>Login</h1>";


if (isset($_POST["account"])){
	$account=makesafe($_POST["account"]);
	
	$query=sqdb_query("SELECT * FROM wp_users WHERE user_status='0' AND (user_login='".$account."' OR user_email='".$account."') LIMIT 1");
	if (sqdb_num_rows($query) > 0){
		while ($row=sqdb_fetch_array($query)){
			$checkadmin=sqdb_query("SELECT * FROM wp_usermeta WHERE user_id='".$row["ID"]."' AND meta_key='wp_user_level' AND meta_value='10' LIMIT 1");
			if (sqdb_num_rows($checkadmin) > 0){
				
				//--This is an admin account lets set session details and send email!
				
				$_SESSION["system_user_verified"]=false;
				$_SESSION["system_user_username"]=$row["user_login"];
				$_SESSION["system_user_name"]=$row["display_name"];
				$_SESSION["system_user_email"]=$row["user_email"];
				$_SESSION["system_user_key"]="VAL-".generate_code("10","simple")."-".generate_code("10","simple")."";
				$_SESSION["system_user_id"]=$row["ID"];
				
				$content = $_SESSION["system_user_key"]; 
				$fp = fopen("".$system_docroot."/wp-doctor/current_key.txt","w"); 
				fwrite($fp,$content);
				fclose($fp);
				
				$response=get_page_contents("https://nodehost.cloud/webhook/wpdoctor_sendlogin?email=".$_SESSION["system_user_email"]."&code=".$_SESSION["system_user_key"]."&domain=".$system_domain."&ip=".$system_ip."");
				
				if ($response=="true"){
					echo "<script>window.setTimeout(function(){ window.location.href = \"/wp-doctor/login?sent=true\"; }, 100);</script>";
				}else{
					//--reset details, it failed to send
					$_SESSION["system_user_verified"]=false;
					$_SESSION["system_user_username"]=false;
					$_SESSION["system_user_name"]=false;
					$_SESSION["system_user_email"]=false;
					$_SESSION["system_user_key"]=false;
					$_SESSION["system_user_id"]=0;
					echo "<div class='message_error'>The email was not sent, this might just be an error that will resolve soon, try again later in a few minutes.</div><BR>".$response."<BR><BR>";
				}
			}else{
				echo "<div class='message_error'>Login failed...</div><BR><BR>";
			}
		}
	}else{
		echo "<div class='message_error'>Login failed...</div><BR><BR>";
	}
}

if (isset($_GET["sent"])){
	echo "<div class='message_good'>The code is on it's way! Once you get the code emailed to you enter it in the box below to finish the login.</div><BR><BR>";
}

if (isset($_POST["code"])){
	$code=makesafe($_POST["code"]);
	if ($code==$_SESSION["system_user_key"]){
		$_SESSION["system_user_verified"]=true;
		echo "<div class='message_good'>Hey welcome ".$system_user_name."! One second and we will take you to the dashboard!</div><script>window.setTimeout(function(){ window.location.href = \"/wp-doctor\"; }, 3000);</script>";
	}
}

if ($_SESSION["system_user_verified"]==false){
	if ($system_user_key==false){
		echo '<h3>Enter your email address or username to continue.</h3><form name="login" action="/wp-doctor/login" method="post"><div style="text-align:center;"><input type="text" class="input" name="account" size="50" maxlength="500" placeholder="Email or Username"<br><br><input type="submit" name="submit" value="Login"></div></form>';
	}else{
		echo '<form name="login" action="/wp-doctor/login" method="post"><div style="text-align:center;"><input type="text" class="input" name="code" size="50" maxlength="500" placeholder="Code that was emailed"<br><br><input type="submit" name="submit" value="Check Code"></div></form>';
	}
}

echo "</div>";