<?php
//--#############################################################
//--############################################################# -- Startup
//--#############################################################

session_name("wp-doctor");
session_start();

set_time_limit(600);
ignore_user_abort(true);
ini_set('output_buffering', 0);
ini_set('display_errors', '0');
ob_start('ob_gzhandler');

include("sqdb.php");
include("functions.php");
include("functions_wordpress.php");
include("functions_internal.php");

//--#############################################################
//--############################################################# -- Basic browser details (This gives constant variable names)
//--#############################################################

$system_ip=get_trueip();
$system_http="http://";
$system_useragent=makesafe($_SERVER['HTTP_USER_AGENT']);
$system_domain=makesafe($_SERVER['HTTP_HOST']);
$system_timestamp=date('YmdHis');
$system_folderstamp=date('Y-m');
$system_docroot=$_SERVER["DOCUMENT_ROOT"];


//--#############################################################
//--############################################################# -- Fetch and variables we may need, or set defaults
//--#############################################################

//--Load user variables, yes it can be done cleaner but this make it easer to read for faster work
if (isset($_SESSION["system_user_verified"])){
	$system_user_verified=$_SESSION["system_user_verified"];
	$system_user_username=$_SESSION["system_user_username"];
	$system_user_name=$_SESSION["system_user_name"];
	$system_user_email=$_SESSION["system_user_email"];
	$system_user_key=$_SESSION["system_user_key"];
	$system_user_id=$_SESSION["system_user_id"];
}else{
	//--We need to set some defaults as we don't have anything set yet
	$_SESSION["system_user_verified"]=false;
	$_SESSION["system_user_username"]=false;
	$_SESSION["system_user_name"]=false;
	$_SESSION["system_user_email"]=false;
	$_SESSION["system_user_key"]=false;
	$_SESSION["system_user_id"]=0;
}

//--#############################################################
//--############################################################# -- URL info
//--#############################################################

if (strpos($_SERVER['REQUEST_URI'], '?') !== false) {
	$system_uri=makesafe(substr($_SERVER['REQUEST_URI'], 0, strrpos( $_SERVER['REQUEST_URI'], "?")));
}else{
	$system_uri=makesafe($_SERVER['REQUEST_URI']);
}

if (isset($_GET["loginsendvalidate"])){
	$code=makesafe($_GET["loginsendvalidate"]);
	$current=file_get_contents("".$system_docroot."/wp-doctor/current_key.txt", FILE_USE_INCLUDE_PATH);
	unlink("".$system_docroot."/wp-doctor/current_key.txt");
	if ($current==$code){
		echo "ok";
	}else{
		echo "fail ".$code." does not match ".$current."";
	}
	die();
}

if ($system_uri=="/wp-doctor/"){
	if ($system_user_verified==true){
		header("Location: /wp-doctor/dashboard");
		die();
	}else{
		header("Location: /wp-doctor/login");
		die();
	}
}

//--Validate if we are or are not using https, due to different server configs lets check a few ways!
$system_http="http://";
if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] == 'on') {
	$system_http = "https://";
}
if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == "https" || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == "on") {
	$system_http = "https://";
}
if (!empty($_SERVER['HTTP_X_FORWARDED_PROTO']) && $_SERVER['HTTP_X_FORWARDED_PROTO'] == "http" || !empty($_SERVER['HTTP_X_FORWARDED_SSL']) && $_SERVER['HTTP_X_FORWARDED_SSL'] == "off") {
	$system_http = "http://";
}
if (!empty($_SERVER['REQUEST_SCHEME']) && $_SERVER['REQUEST_SCHEME'] == "http"){
	$system_http="http://";
}

//--Only redirect if we have a valid ssl cert first
if ($system_http=="http://"){
	if (check_has_ssl($system_domain)==true){
		$redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
		header('HTTP/1.1 301 Moved Permanently');
		header('Location: ' . $redirect);
		exit();
	}
}

//--#############################################################
//--############################################################# -- Load Wordpress details from config file
//--#############################################################

//--Check session data, start database connection by getting data from database
if (!isset($_SESSION["system_db"])){
	$configfile = file_get_contents('../wp-config.php', FILE_USE_INCLUDE_PATH);
	$_SESSION["system_db"]=true;
	$_SESSION["system_db_server"]=get_regix_string("/DB_HOST\'\,\ \'([^\n\s]+)'/", $configfile);
	$_SESSION["system_db_username"]=get_regix_string("/DB_USER\'\,\ \'([^\n\s]+)'/", $configfile);
	$_SESSION["system_db_password"]=get_regix_string("/DB_PASSWORD\'\,\ \'([^\n\s]+)'/", $configfile);
	$_SESSION["system_db_database"]=get_regix_string("/DB_NAME\'\,\ \'([^\n\s]+)'/", $configfile);
}

//--#############################################################
//--############################################################# -- Load in database details we have
//--#############################################################

if (isset($_SESSION["system_db"])){
	sqdb_setlogin($_SESSION["system_db_server"],$_SESSION["system_db_username"],$_SESSION["system_db_password"],$_SESSION["system_db_database"]);
}else{
	die("Database connection is not found");
}

//--#############################################################
//--############################################################# -- Start pages and content
//--#############################################################

include("theme_top.html");
echo "<div class='case'>";

if ($system_user_verified==true){
	echo "<div class='menu'>";
	
	echo "<h2 style='margin-top:0px;margin-bottom:5px;'>".get_value_wp_options('blogname')."</h2>";
	echo "<h3 style='margin-top:0px;'>".get_value_wp_options('blogdescription')."</h3>";
	
	echo "<h4>Main</h4>";
	echo "<a href='/wp-doctor/dashboard'>Dashboard</a>";
	
	echo "<h4>Debug</h4>";
	echo "<a href='/wp-doctor/debug_admin_access'>WP-Admin Access</a>";
	
	echo "<h4>Options</h4>";
	echo "<a href='/wp-doctor/logout'>Logout</a>";
	echo "<a href='/wp-doctor/update_testrun73j83883'>Update Now (testing)</a>";
	
	echo "</div>";
	echo "<div class='content'><div class='bubble'>";
}else{
	echo "<div class='bubble small'>";
}

//--This is here only during building of the system
if ($system_uri=="/wp-doctor/update_testrun73j83883"){ include "update.php"; }

if ($system_user_verified==false){
	if ($system_uri=="/wp-doctor/login"){ include "pages/login.php"; }
}else{
	if ($system_uri=="/wp-doctor/dashboard"){ include "pages/dashboard.php"; }
	if ($system_uri=="/wp-doctor/logout"){ include "pages/logout.php"; }
	
	
	if ($system_uri=="/wp-doctor/debug_admin_access"){ include "pages/debug_admin_access.php"; }
}

if ($system_user_verified==true){
	echo "</div></div>";
}else{
	echo "</div>";
}
echo "</div>";
echo "<div class='footer'>Domain: ".$system_domain." | IP: ".$system_ip."</div>";

include("theme_bottom.html");
