<?php
	
//--#############################################################
//--############################################################# -- GET
//--#############################################################

function get_regix_string($string,$data){
	$check_hash = preg_match_all($string, $data, $find);
	foreach ($find[1] as $ht){
	  return $ht;
	}
}

function get_trueip(){
	$ip="0.0.0.0";
	if (isset($_SERVER['HTTP_X_FORWARDED_FOR'])){ if (filter_var($_SERVER['HTTP_X_FORWARDED_FOR'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_X_FORWARDED_FOR']; }}}
	if (isset($_SERVER['HTTP_CF_CONNECTING_IP'])){ if (filter_var($_SERVER['HTTP_CF_CONNECTING_IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_CF_CONNECTING_IP']; }}}
	if (isset($_SERVER['Cf-Connecting-IP'])){ if (filter_var($_SERVER['Cf-Connecting-IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['Cf-Connecting-IP']; }}}
	if (isset($_SERVER['HTTP_CLIENT_IP'])){ if (filter_var($_SERVER['HTTP_CLIENT_IP'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['HTTP_CLIENT_IP']; }}}
	if (isset($_SERVER['REMOTE_ADDR'])){ if (filter_var($_SERVER['REMOTE_ADDR'], FILTER_VALIDATE_IP)){ if ($ip=="0.0.0.0"){ $ip=$_SERVER['REMOTE_ADDR']; }}}
	return $ip;
}

function get_percentage($total, $number){
	if ( $total > 0 ) {
		return round($number / ($total / 100),2);
	} else {
		return 0;
	}
}

function get_page_contents($url){
	$process = curl_init($url);
	curl_setopt($process, CURLOPT_HEADER, 0);
	curl_setopt($process, CURLOPT_TIMEOUT, 30);
	curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($process, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; WP Doctor/1.13; +https://nodehost.ca/wp-doctor)');
	curl_setopt($process, CURLOPT_FAILONERROR, true);
	curl_setopt($process, CURLOPT_AUTOREFERER, true);
	curl_setopt($process, CURLOPT_MAXREDIRS, 7);
	
	$return = curl_exec($process);
	if (curl_error($process)){
		error_log(curl_error($process));
	}
	curl_close($process);
	return $return;
}

function get_page_redirect($url){
	$process = curl_init($url);
	curl_setopt($process, CURLOPT_HEADER, 0);
	curl_setopt($process, CURLOPT_TIMEOUT, 30);
	curl_setopt($process, CURLOPT_HEADER, true);
	curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($process, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($process, CURLOPT_SSL_VERIFYPEER, false);
	curl_setopt($process, CURLOPT_USERAGENT, 'Mozilla/5.0 (compatible; WP Doctor/1.13; +https://nodehost.ca/wp-doctor)');
	curl_setopt($process, CURLOPT_FAILONERROR, true);
	curl_setopt($process, CURLOPT_AUTOREFERER, true);
	curl_setopt($process, CURLOPT_MAXREDIRS, 1);
	curl_setopt($process, CURLOPT_NOBODY, true);
	
	$return = curl_exec($process);
	$return = str_replace("\r", "", $return);
	
	$headers = explode("\n", $out);
	foreach($headers as $header){
		if(substr($header, 0, 10) == "Location: "){
			$target = substr($header, 10);
			return $target;
		}
	}
	
	return false;
	
}

function get_log($name){
	global $system_docroot;
	return file_get_contents("".$system_docroot."/wp-doctor/logs/".$name.".log", FILE_USE_INCLUDE_PATH);
}

//--#############################################################
//--############################################################# -- WRITE
//--#############################################################

function write_log($line,$name){
	global $system_docroot;
	error_log("".$line."\n", 3, "".$system_docroot."/wp-doctor/logs/".$name.".log");
}


//--#############################################################
//--############################################################# -- DELETE
//--#############################################################

function delete_log($name){
	global $system_docroot;
	unlink("".$system_docroot."/wp-doctor/logs/".$name.".log");
}

//--#############################################################
//--############################################################# -- CHECK
//--#############################################################

function check_has_ssl($domain){
	$res = false;
	$orignal_parse = $domain;
	
	$wildcard_explode=explode(".", $domain);
	$wildcard_explode[0]="*";
	$wildcard=implode(".", $wildcard_explode);
	
	$stream = @stream_context_create( array( 'ssl' => array( 'capture_peer_cert' => true ) ) );
	$socket = @stream_socket_client( 'ssl://' . $orignal_parse . ':443', $errno, $errstr, 30, STREAM_CLIENT_CONNECT, $stream );

	if ($socket){
		$cont = stream_context_get_params( $socket );
		$cert_ressource = $cont['options']['ssl']['peer_certificate'];
		$cert = openssl_x509_parse( $cert_ressource );
		$listdomains=explode(',', $cert["extensions"]["subjectAltName"]);

		foreach ($listdomains as $v) {
			if (strpos($v, $orignal_parse) !== false) {
				$res=true;
			}
			if (strpos($v, $wildcard) !== false) {
				$res=true;
			}
		}
	}
	return $res;
}