<?php
	
function generate_code($length,$type){
	if ($length<=0){
		$length=10;
	}
	if ($type=="normal"){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyz';
	}
	if ($type=="simple"){
		$characters = '0123456789abcabcabc';
	}
	if ($type=="password"){
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ-_-_-_-_$$%%#@@!!((*&&^^%%@&^$T^(!++++';
	}
	$randomString = '';
	for ($i = 0; $i < $length; $i++) {
		$randomString .= $characters[rand(0, strlen($characters) - 1)];
	}
	return $randomString;
}

function makesafe($d,$type="basic"){
	$d = str_replace("\t","~~",$d);
	$d = str_replace("\r","",$d);
	$d = str_replace("\n","	",$d);
	$d = str_replace("|","&#124;",$d);
	$d = str_replace("\\","&#92;",$d);
	$d = str_replace("(c)","&#169;",$d);
	$d = str_replace("(r)","&#174;",$d);
	$d = str_replace("\"","&#34;",$d);
	$d = str_replace("'","&#39;",$d);
	$d = str_replace("<","&#60;",$d);
	$d = str_replace(">","&#62;",$d);
	$d = str_replace("+","&#43;",$d);
	$d = str_replace("`","&#96;",$d);
	$d = str_replace("DELETE FROM","",$d);
	return $d;
}